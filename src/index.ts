import {ApplicationConfig, TareaIntroAlgoritmoApplication} from './application';

export * from './application';

export async function main(options: ApplicationConfig = {}) {
  const app = new TareaIntroAlgoritmoApplication(options);
  await app.boot();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}

if (require.main === module) {
  // Run the application
  const config = {
    rest: {
      port: +(process.env.PORT ?? 3000),
      host: process.env.HOST,
      // The `gracePeriodForClose` provides a graceful close for http/https
      // servers with keep-alive clients. The default value is `Infinity`
      // (don't force-close). If you want to immediately destroy all sockets
      // upon stop, set its value to `0`.
      // See https://www.npmjs.com/package/stoppable
      gracePeriodForClose: 5000, // 5 seconds
      openApiSpec: {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true,
      },
    },
  };
  main(config).catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });

  var arrayMonths= ['Diciembre','Febrero','Agosto','Septiembre','Julio','Enero','Junio','Octubre','Marzo','Mayo','Abril','Noviembre'];
var month;
    for(var j=0;j<arrayMonths.length;j++){
    for(var i=0;i<arrayMonths.length;i++){
    switch (arrayMonths[i]) {
        case 'Enero':
            orderMonth(0,i);
          break;
        case 'Febrero':
            orderMonth(1,i);
          break;
        case 'Marzo':
            orderMonth(2,i);
          break;
        case 'Abril':
            orderMonth(3,i);
          break;
        case 'Mayo':
            orderMonth(4,i);
          break;
        case 'Junio':
            orderMonth(5,i);
          break;
        case 'Julio':
            orderMonth(6,i);
            break;
        case 'Agosto':
            orderMonth(7,i);
            break;
        case 'Septiembre':
            orderMonth(8,i);
          break;
        case 'Octubre':
            orderMonth(9,i);
            break;
        case 'Noviembre':
            orderMonth(10,i);
            break;
        case 'Diciembre':
            orderMonth(11,i);
            break;
          default:
        }}    }

function orderMonth(numberMes:number,numberOrden:number) {
        month=arrayMonths[numberMes];
        arrayMonths[numberMes]=arrayMonths[numberOrden];
        arrayMonths[numberOrden]=month;

  }
console.log(arrayMonths);

}
